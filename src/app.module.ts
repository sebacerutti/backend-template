import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import * as dotenv from "dotenv";
import { AuthModule } from './auth/auth.module';
import { UsuariosModule } from './usuarios/usuarios.module';

dotenv.config();

@Module({
  imports: [
    MongooseModule.forRoot(`mongodb+srv://${process.env.USERNAME_DB}:${process.env.PASSWORD_DB}@api-href.oxq5or0.mongodb.net/${process.env.DB_NAME}`),
    AuthModule,
    UsuariosModule,
  ],
  controllers:[],
  providers: [],
})
export class AppModule {}
