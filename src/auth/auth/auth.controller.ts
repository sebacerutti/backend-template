import { Body, Controller, Get, Post, Req, UnauthorizedException, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { UsuariosService } from 'src/usuarios/services/usuarios.service';
import { Request } from 'express';
import { UserDTO } from 'src/usuarios/models/usuarios.model';

@Controller('auth')
export class AuthController {
  constructor(
    private authService: AuthService,
    private usariosService: UsuariosService
    ) {}

  @Get("verify-token")
  @UseGuards(AuthGuard("jwt"))
  async verifyToken(
    @Req() req:Request
  ){
    return { message: "Token aún válido.", user: req.user }
  }

  @Post('login')
  async login(
    @Body() bodyLogin: UserDTO
    ) {
    const user = await this.authService.validateUser(bodyLogin.email, bodyLogin.password);

    if(!user) throw new UnauthorizedException("Credenciales inválidas.");

    return this.authService.login(user);
  }

  @Post('register')
  async register(
    @Body() body: UserDTO
  ) {
    const existingUser = await this.usariosService.findByEmail(body.email);
    if(existingUser) throw new UnauthorizedException("El correo electrónico ya está en uso.");

    const newUser = await this.usariosService.create(body);
    return newUser;
  }

}