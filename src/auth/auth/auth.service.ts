import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { User } from 'src/usuarios/models/usuarios.model';
import { UsuariosService } from 'src/usuarios/services/usuarios.service';
import * as bcrypt from "bcrypt";

@Injectable()
export class AuthService {
  constructor(
    private usuariosServie: UsuariosService,
    private jwtService: JwtService
  ) {}

  async validateUserFromPayload(payload: any): Promise<User | null> {
    const email = payload.email;
    const user = await this.usuariosServie.findByEmail(email);
    return user || null;
  }

  async login(user: User) {
    const payload = { sub: user._id, email: user.email };
    const access_token = this.jwtService.sign(payload);
    return { access_token };
  }

  async validateUser(email: string, password: string): Promise<User | null> {
    const user = await this.usuariosServie.findByEmail(email);

    if (user && (await this.comparePasswords(password, user.password))) {
      return user;
    }
    return null;
  }

  private async comparePasswords(
    inputPassword: string,
    hashedPassword: string
  ): Promise<boolean> {
    return bcrypt.compare(inputPassword, hashedPassword);
  }
}
