import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();

  const config = new DocumentBuilder()
    .setTitle("API - Herrajeria Yacante")
    .setDescription("Breve explicacion del correcto uso de este backend")
    .setVersion("2.0.0")
    .build();

  const document = SwaggerModule.createDocument(app,config);
  SwaggerModule.setup('api-docs', app, document)
  await app.listen(4001);
}
bootstrap();
