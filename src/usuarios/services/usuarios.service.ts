import { BadRequestException, ConflictException, Injectable } from '@nestjs/common';
import { User, UserDTO } from '../models/usuarios.model';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Injectable()
export class UsuariosService {
  constructor(
    @InjectModel('User') private readonly userModel: Model<User>,
  ) {}

  async create(user: UserDTO): Promise<User> {
    if(!user.email || !user.name || !user.password) throw new BadRequestException('Alguno de los campos requeridos no fue brindado');
    if (await this.userModel.findOne({ email: user.email })) {
      throw new ConflictException('El usuario ya existe');
    }
    const userToCreate = {
      ...user,
      enabled: true
    };
    const createdUser = await this.userModel.create(userToCreate);
    await createdUser.save();
    return createdUser;
  }

  async findByEmail(email: string): Promise<User | null> {
    return this.userModel.findOne({ email: email }).exec() || null;
  }
}
