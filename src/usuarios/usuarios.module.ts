import { Module } from '@nestjs/common';
import { UsuariosService } from './services/usuarios.service';
import { UserSchema } from './models/usuarios.model';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    MongooseModule.forFeature([
      {name: "User", schema: UserSchema},
    ])
  ],
  providers: [UsuariosService],
  controllers: [],
  exports: [UsuariosService]
})
export class UsuariosModule {}
